/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    fontSize: {
      '8xl': [
        '120px',
        {
          lineHeight: '120px',
          letterSpacing: '-6px',
          fontWeight: '500',
        }
      ],
      '7xl': [
        '72px',
        {
          lineHeight: '80px',
          letterSpacing: '-4.5px',
          fontWeight: '600',
        }
      ],
      '6xl': [
        '55px',
        {
          lineHeight: '60px',
          letterSpacing: '-2.5px',
          fontWeight: '500',
        }
      ],
      '5xl': [
        '48px',
        {
          lineHeight: '54px',
          letterSpacing: '-1.61px',
          fontWeight: '500',
        }
      ],
      '4xl': [
        '36px',
        {
          lineHeight: '44px',
          letterSpacing: '-1.3px',
          fontWeight: '500',
        }
      ],
      '3xl': [
        '30px',
        {
          lineHeight: '36px',
          letterSpacing: '-0.8px',
          fontWeight: '500',
        }
      ],
      
      '2xl': [
        '24px',
        {
          lineHeight: '32px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],
      
      'xl': [
        '20px',
        {
          lineHeight: '28px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],
       
      'lg': [
        '18px',
        {
          lineHeight: '24px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],

      'base': [
        '16px',
        {
          lineHeight: '24px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],

      'sm': [
        '14px',
        {
          lineHeight: '20px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],

      'xs': [
        '12px',
        {
          lineHeight: '16px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],

      'xxs': [
        '10px',
        {
          lineHeight: '16px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],
      'caption-lg': [
        '20px',
        {
          lineHeight: '28px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],
      'caption-base': [
        '18px',
        {
          lineHeight: '24px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],
      'caption-sm': [
        '16px',
        {
          lineHeight: '24px',
          letterSpacing: '-0.5px',
          fontWeight: '400',
        }
      ],

      
    },
    extend: {
      colors: {
        'primary': '#FF5A5F',
        'secondary': '#00A699',
        'tertiary': '#007D8C',
        'gray': '#767676',
        'dark': '#222222',
        'light': '#FFFFFF',
        'light-gray': '#F2F2F2',
        'accent': '#FFB81C',
        'success': '#56A64B',
        'warning': '#F5A653',
        'danger': '#D0021B',
        
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
    },
  },
  plugins: [],
}
