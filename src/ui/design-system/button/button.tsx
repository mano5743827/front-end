import clsx from "clsx";
import { InconProps } from "@/types/iconProps";
import { Spinner } from "@/ui/design-system/spinner/Spinner";

interface ButtonProps {
    variant?: "primary" | "secondary" | "accent"
    | "danger" | "warning" | "success" | "light" | "dark" | "gray"|"ico";
    size?: "sm" | "md" | "lg";
    children?: React.ReactNode;
    className?: string;
    onClick?: () => void;
    disabled?: boolean;
    icon?: InconProps;
    iconTheme?: "light" | "dark" | "gray" | "primary" | "secondary" | "accent";
    type?: "button" | "submit" | "reset";
    theme?: "light" | "dark" | "gray" | "primary" | "secondary" | "accent";
    weight?: "regular" | "medium" | "bold";
    component?: "button" | "a";
    href?: string;
    isLoading?: boolean;
    iconPosition?: "left" | "right";
    target?: "_blank" | "_self" | "_parent" | "_top";
    rel?: "alternate" | "author" | "bookmark" | "external" | "help" | "license" | "next" | "nofollow" | "noreferrer" | "noopener" | "prev" | "search" | "tag";
}


export const Button = ({
    variant = "primary",
    size = "md",
    children,
    className,
    onClick,
    disabled,
    iconPosition = "left",
    icon,
    isLoading,
    iconTheme = "secondary",
    type = "button",
    theme = "dark",
    component: Component = "button",
}: ButtonProps) => {
    

    let variantStyles: string = "", sizeStyles: string = "",
        positionStyles: string = ""
        
        ;

    switch (variant) {
        case "primary":
            variantStyles = "bg-primary ";
            break;
        case "secondary":
            variantStyles = "bg-secondary text-white rounded-lg";
            break;
        case "accent":
            variantStyles = "bg-accent text-white";
            break;
        case "danger":
            variantStyles = "bg-danger text-white";
            break;
        case "warning":
            variantStyles = "bg-warning text-white rounded-lg hover:bg-gold-900";
            break;
        case "success":
            variantStyles = "bg-success  hover:bg-red-600 rounded-lg text-white";
            break;
        case "ico":
            if (iconTheme === "secondary") { 
                variantStyles = "bg-secondary text-white rounded-full";
            }
            break;
    }

    switch (type) {
        case "button":
            type = "button";
            break;
        case "submit":
            type = "submit";
            break;
        case "reset":
            type = "reset";
            break;
        
    }

    switch (size) {
        case "sm":
            sizeStyles = "px-4 py-2 text-sm";
            break;
        case "md":
            sizeStyles = "px-6 py-3 text-base";
            break;
        case "lg":
            sizeStyles = "px-8 py-4 text-lg";
            break;
    }

    switch (iconPosition) {
        case "left":
            positionStyles = "relative left-2";
            break;
        case "right":
            positionStyles = "absolute right-2";
            break;
    }

    



    return (
        <Component className={clsx(variantStyles, sizeStyles, className,
            positionStyles,
            isLoading && "cursor-wait opacity-50 inset-0",
            "absolute"
        )}
            onClick={() => console.log("Button Clicked")}
            disabled={disabled}
            
            
        >

            {isLoading && (
                <div className="absolute inset-0 flex items-center justify-center">
                    {variant === "accent" || variant === "ico" ? (
                        <Spinner variant="white" />):(
                            <Spinner size="sm"/>
                    )}
                    
                </div>
            )}

            <div className={clsx(isLoading && "flex item-center gap-1")}>
                {icon && variant ==="ico" ? <icon.icon/> : <>{children}</>}
            </div>

            
            
        </Component>
    )
}