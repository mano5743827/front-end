import clsx from "clsx";

interface TypographyProps {
    variant?:
    |"display"
    | "h1"
    | "h2"
    | "h3"
    | "body-lg"
    | "body-base"
    | "body-sm"
    | "body-xs"
    | "caption-lg"
    | "caption-base"
    | "caption-sm"
    |"lead";
    children: React.ReactNode;
    component?: "h1" | "h2" | "h3" | "h4" | "h5" | "span" | "p" | "div";
    theme?: "light" | "dark" | "gray" | "primary" | "secondary" | "accent";
    weight?: "regular" | "medium" | "bold";
    className?: string;
    
}

export const Typography = ({
    variant = "h3",
    children,
    theme = "dark",
    weight = "regular",
    className,
    component: Component = "div" }: TypographyProps) => {
    
        let variantStyles: string = "", colorStyles: string="";
    
        switch (variant) {
            case "display":
                variantStyles = "text-8xl";
                break;
            case "h1":
                 variantStyles = " text-7xl";
                break;
            case "h2":
                variantStyles = " text-6xl";
                break;
            case "h3":
                variantStyles = " text-5xl";
                break;
            case "body-lg":
                variantStyles = " text-lg";
                break;
            case "body-base":
                variantStyles = " text-base";
                break;
            case "body-sm":
                variantStyles = " text-sm";
                break;
            case "body-xs":
                variantStyles = " text-xs";
                break;
            case "caption-lg":
                variantStyles = " text-lg";
                break;
            case "caption-base":
                variantStyles = " text-base";
                break;
            case "caption-sm":
                variantStyles = " text-sm";
                break;
            
        }

    switch (theme) {
        case "light":
            colorStyles = " text-white";
            break;
        case "dark":
            colorStyles = " text-black";
            break;
        case "gray":
            colorStyles = " text-gray-500";
            break;
        case "primary":
            colorStyles = " text-primary";
            break;
        case "secondary":
            colorStyles = " text-secondary";
            break;
        case "accent":
            colorStyles = " text-accent";
            break;
    }

    switch (weight) {
        case "regular":
            colorStyles = " font-normal";
            break;
        case "medium":
            colorStyles = " font-medium";
            break;
        case "bold":
            colorStyles = " font-bold";
            break;
    }
    
    return (
        <Component
            className={clsx(variantStyles, colorStyles,
            
                weight === "regular" && "font-medium",
                className, 
            )}
        >
            {children}
        </Component>
    );
};

