interface SpinnerProps {
    size?: "sm" | "md" | "lg";
    variant?: "primary" | "secondary" | "white";  
}



export const Spinner = ({ size = "md", variant ="primary" }: SpinnerProps) => {

    let sizeStyles = "";
    let variantStyles = "";

    switch (size) {
        case "sm":
            sizeStyles = "h-5 w-5";
            break;
        case "md":
            sizeStyles = "h-6 w-6";
            break;
        case "lg":
            sizeStyles = "h-7 w-7";
            break;
    }

    switch (variant) {
        case "primary":
            variantStyles = "text-primary";
            break;
        case "secondary":
            variantStyles = "text-secondary";
            break;
        case "white":
            variantStyles = "text-white";
            break;
    }

    return (
        <svg className={`animate-spin ${sizeStyles} ${variantStyles}`} xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
            <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
            <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4z"></path>
        </svg>
    )
}