import Image from "next/image";
import clsx from "clsx";

interface AvatarProps {
    
    size?: 'small' | "very-small" | 'medium' | 'large';
    src: string;
    alt: string;
    className?: string;
}


export const Avatar = ({ size = 'medium', src, alt, className }: AvatarProps) => {

    let logoAvatar :string;

    if (size === 'small') {
        logoAvatar = "w-[24px] h-[24px]";
    } else if (size === 'very-small') {
        logoAvatar = "w-[16px] h-[16px]";
    } else if (size === 'large') {
        logoAvatar = "w-[100px] h-[100px]";
    } else {
        logoAvatar = "w-[50px] h-[50px]";
    }


    return (
        <div className={clsx(
            logoAvatar,
            className ="overflow-hidden bg-gray-400 rounded-full relative"
        )}>
            <Image
                fill
                src={src}
                alt={alt}
                className={`rounded-full object-cover object-center`}
            />
        </div>
    );
}