import clsx from 'clsx';


interface ContainerProps {
    children: React.ReactNode;
    className?: string;
}


export const Container = ({ children, className }: ContainerProps) => {


    return (
        <div className={clsx(className,
            "w-full max-w-7xl py-10 mx-auto space-y-5 lg:px-10"
            
        )}>
            {children}
        </div>
    );
}