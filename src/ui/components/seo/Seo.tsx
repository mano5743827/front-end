import Head from 'next/head';

interface Props {
    title: string;
    description: string;
    viewport?: string;
}

export const Seo = ({title, description, viewport}: Props) => {
    return (
        <>
            <Head>
                <title>{ title }</title>
                <meta name="description" content={description} />
                <meta name="viewport" content={viewport} />
                <link rel="icon" href="/favicon.ico" />
            </Head>    
            
        </>
    )
}
