import Link from 'next/link';
import clsx from 'clsx';
import { useRouter } from 'next/router';
import { useMemo } from 'react';

interface PropsActiveLink {
    href: string;
    children: React.ReactNode;
    className?: string;
}


export const ActiveLink = ({ href, children,className }: PropsActiveLink) => {
    
    const router = useRouter();

    const isActive = useMemo(() => {
        return router.pathname === href;
    }, [router.pathname, href]);

    console.log("isActive", isActive, router.pathname, href);

    return (
        <Link href={href} className={clsx(
            isActive && "text-white bg-slate-500 ",
            className
        )}>
            {children}
        </Link>
     )
}