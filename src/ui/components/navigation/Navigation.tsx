import clsx from 'clsx';
import { Container } from '../container/container';
import { Logo } from '@/ui/design-system/logo/Logo';
import { Typography } from '@/ui/design-system/typography/Typography';
import { Button } from '@/ui/design-system/button/button';
import { ActiveLink } from '@/ui/components/navigation/Active-link';

import Link from 'next/link';

interface NavigationProps {
    children?: React.ReactNode;
    className?: string;
}



export const Navigation = ({  }: NavigationProps) => {
    
        return (
            
            <div className='border-b-2 border-gray-100'>
                <Container className='py-1.5 flex items-center justify-between gap-7'>
                    <Link href="/">  
                    <div className='flex items-center gap-2'>
                    
                        <Logo size='small' />
                        <div className='flex flex-col'>
                        
                            <div className='text-dark font-extrabold text-[24px]'>
                            Coders Mano   
                            </div>
                            <Typography
                                variant='caption-sm'
                                theme='secondary'
                                component='span'
                            >
                            Trouve de l&apos;inspiration pour tes projets
                                </Typography>
                        
                            </div>
                           
                        </div>
                        </Link>
                    <div className='flex items-center gap-7'>
                        <Typography
                            variant='caption-base'
                            component='div'
                            className='flex items-center gap-7'
                        >
                            <ActiveLink href="/design-system">Design Sytème</ActiveLink>
                            <ActiveLink href="/projets">projets</ActiveLink>
                            <ActiveLink href="/formations">Formations</ActiveLink>
                            <ActiveLink href="/contacts">Contacts</ActiveLink>
                        </Typography>
                        <div className='flex items-center gap-2'>
                            <Button size='sm' variant='success'> Connexion </Button>
                            <Button size='sm' variant='warning'> Inscription </Button>
                        </div>
                    </div>
                </Container>
            </div>
        );
}