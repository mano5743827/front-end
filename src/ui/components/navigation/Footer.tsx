import { FooterLinks } from "@/types/appLinks";
import { Typography } from "@/ui/design-system/typography/Typography";
import Image from "next/image";
import { Container } from "../container/container";
import { footerLinks } from "./AppLink";

import { v4 as uuidv4 } from 'uuid';
import { ActiveLink } from "./Active-link";
import { LinkTypes } from "@/lib/link-type";

console.log(footerLinks);

interface FooterProps {
    className?: string;
}

export const Footer = ({ className }: FooterProps) => {

    const currentYear = new Date().getFullYear();

    const footerNavigationList = footerLinks.map((colomnLinks) => (
        <FooterLink key={uuidv4()} data={colomnLinks} />
    ));
        

    return (
        <>
            <div className="bg-dark ">
                <Container className="flex justify-between pt-16">
                    <div className=" flex flex-col items-center text-light">
                        <Typography
                            variant="caption-lg"
                            theme="accent"
                            weight="bold"
                        >Formation gratuite
                        </Typography>
                        <Typography
                            variant="caption-sm"
                            theme="light"
                        >
                            Abonne toi à la chaine
                        </Typography>
                        <a
                            href="https://www.youtube.com/channel/UCzNf0liwUzMN6_pixbQlMhQ"
                            target="_blank">
                            <Image src="/assets/svg/YTB.svg"
                                width={229}
                                height={216}
                                alt="youtube"
                                className="w-20 h-20" />
                        </a>
                    </div>
                    <div className="text-accent flex gap-7">
                       
                    {footerNavigationList}
                        
                    </div>
                </Container>
                <Container >
                    <hr className="text-gray-800" />
                    <div className="flex items-center justify-between">
                        <div className="">
                            <Typography variant="caption-base"
                                theme="light"
                                className="text-light"
                            >
                                {`copyright © ${currentYear} - Tous droits réservés`} 
                                <a
                                    href="https://hnm.haut-numerique.com/"
                                    target="_blank" className="text-accent"
                                > - Haut Numérique Média et services
                                </a>
                            </Typography>
                        </div>
                        <div className=""></div>
                    </div>
                   
                </Container>
            </div>
        </>
    );
}


interface FooterLinkProps { 
    data: FooterLinks;
}


const FooterLink = ({ data }: FooterLinkProps) => {
    console.log(data);

    const linkList = data.links.map((link) => (
        <div key={uuidv4()}>
            
            {link.type === LinkTypes.INTERNAL && (
               <ActiveLink  href={link.baseUrl} className="text-accent">
               {link.label} 
                </ActiveLink> 
            )}
            
            {link.type === LinkTypes.EXTERNAL && (
                <a href={link.baseUrl}
                className="text-accent"
                target="_blank"
                >
                    {link.label}
                 </a>
            )}
            
         </div>
         
        
    ));

    return (
        <div className="min-w-[190px]">
            <Typography
                variant="caption-lg"
                theme="light"
                weight="bold"
                className="pb-4"
            >
            {data.label}
            </Typography>
            <Typography
                variant="caption-sm"
                theme="light"
                className="space-y-4"
            >

                {linkList}
            </Typography>
        </div>
    )
}