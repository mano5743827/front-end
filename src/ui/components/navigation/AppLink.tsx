import { AppLinks, FooterLinks } from "@/types/appLinks";



const footerApplicationLinks: AppLinks[] = [
    {
        label: "Accueil",
        baseUrl: "/",
        type: "internal",

    },
    {
        label: "Projets",
        baseUrl: "/",
        type: "internal",

    },
    {
        label: "Coders Mano",
        baseUrl: "/",
        type: "internal",

    },
    {
        label: "Formations",
        baseUrl: "https://hnm.haut-numerique.com/",
        type: "external",

    },
];

const footerUserLinks: AppLinks[] = [
    {
        label: "Mon espace",
        baseUrl: "/",
        type: "internal",

    },
    {
        label: "Connexion",
        baseUrl: "/",
        type: "internal",

    },
    {
        label: "Inscription",
        baseUrl: "/",
        type: "internal",

    },
    {
        label: "Mot de passe oublié",
        baseUrl: "https://hnm.haut-numerique.com/",
        type: "internal",

    },
];

const footerInformationLinks: AppLinks[] = [
    {
        label: "CGU",
        baseUrl: "/",
        type: "internal",

    },
    {
        label: "Confidentialité",
        baseUrl: "/",
        type: "internal",

    },
    {
        label: "A propos",
        baseUrl: "/",
        type: "internal",

    },
    {
        label: "Contact",
        baseUrl: "https://hnm.haut-numerique.com/",
        type: "internal",

    },
];

const footerReseauxLinks: AppLinks[] = [
    {
        label: "Linkedin",
        baseUrl: "/",
        type: "external",

    },
    {
        label: "Youtube",
        baseUrl: "/",
        type: "external",

    },
    {
        label: "Slack",
        baseUrl: "/",
        type: "external",

    },
    
];


export const footerLinks:FooterLinks[] = [
    {
        label: "App",
        links: footerApplicationLinks,
    },
    {
        label: "Utilisateurs",
        links: footerUserLinks,
    },
    {
        label: "Information",
        links: footerInformationLinks,
    },
    {
        label: "Réseaux",
        links: footerReseauxLinks,
    }
]

