import { Seo } from '@/ui/components/seo/Seo'
import { Typography } from '@/ui/design-system/typography/Typography'
import { Button } from '@/ui/design-system/button/button'
import Head from 'next/head'
import { RiAncientGateFill } from "react-icons/ri";
import { Spinner } from '@/ui/design-system/spinner/Spinner';
import { Logo } from '@/ui/design-system/logo/Logo';
import { Avatar } from '@/ui/design-system/avatar/Avatar';
import { Container } from '@/ui/components/container/container';
import { Navigation } from '@/ui/components/navigation/Navigation';
import { Footer } from '@/ui/components/navigation/Footer';



export default function Home() {
  return (
    <>
      
      <Seo title="Create Next App"
        description='Next.js + TypeScript + Tailwind CSS'
        viewport='width=device-width, initial-scale=1'
      />
      <Navigation />
      <Footer />
    
    </>
    
  )
}
