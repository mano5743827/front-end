import { Container } from "@/ui/components/container/container";
import { Navigation } from "@/ui/components/navigation/Navigation";
import { Seo } from "@/ui/components/seo/Seo";
import { Avatar } from "@/ui/design-system/avatar/Avatar";
import { Button } from "@/ui/design-system/button/button";
import { Logo } from "@/ui/design-system/logo/Logo";
import { Typography } from "@/ui/design-system/typography/Typography";
import { RiAncientGateFill } from "react-icons/ri";

export default function DesignSystem() {
    return (
       <>
                <Seo title="Design système"
                  description='Design système'
                  viewport='width=device-width, initial-scale=1'
                />
                <Navigation />
              <Container className="space-y-10"> 
                <div className='space-y-2'>
                  <Typography component='div'>
                    Text 
                  </Typography>

                    <div className='flex space-x-2 border border-green-400 rounded'>
                    <Typography variant='h1' component='span' theme='secondary'>
                          Next.js + TypeScript + Tailwind CSS
                    </Typography>
                      </div>
                  
                </div>
                
                
                <div className='space-y-2'>
                  <Typography component='div'>
                    Bouton
                    
                    </Typography>
                    <div className='flex space-x-2 border border-green-400 rounded'>
                        <Button variant='success' type='button' size='md'>
                      click me
                        </Button>
                        <Button variant='secondary' type='reset' size='lg' iconPosition='right'>
                      click me 2
                    </Button>
                      <Button
                    variant='ico' type='submit'
                    size='lg' iconTheme='secondary'
                    icon={{ icon: RiAncientGateFill }}
                    isLoading
                        />
                  </div>
                  
                </div>
                
                
                

                
                <div className='space-y-2'>
                <Typography component='div'>
                  Logo
                  
                  </Typography>
                  <div className='flex space-x-2 border border-green-400 rounded'>
                      <Logo size='large'/>
                      <Logo />
                    <Logo size='small' />
                    <Logo size='very-small' />
                    
                  </div>
                  
                </div>
                
                <div className='space-y-2'>
                  <Typography component='div'>
                    Avatar
                    
                    </Typography>
                    <div className='flex space-x-2 border border-green-400 rounded'>
                        <Avatar size='large' src="/assets/images/md2.jpg" alt="Picture of the author"/>
                        <Avatar src="/assets/images/md.png" alt="Picture of the author"/>
                      <Avatar size='small' src="/assets/images/md.png" alt="Picture of the author"/>
                      <Avatar size='very-small' src="/assets/images/md.png" alt="Picture of the author"/>
                      
                  </div>
                  
                </div>
              </Container> 
       </>
    )
}